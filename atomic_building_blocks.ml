(** The material system*)

type guard_atom = {
    name: string;
    mutable x:float; mutable y:float; mutable z:float;
}

type atom = {
    name: string;
    mutable dx:float; mutable dy:float; mutable dz:float;
}

type space_idx = {mutable ix:int; mutable iy:int; mutable iz:int}

(*The primitive cell; the first atom is the special one, setting local strain*)
type pcell = {
    mutable a0:guard_atom;
    mutable other:atom list;
    mutable idx:space_idx
}

type irprimd = {
    mutable ia1:space_idx;
    mutable ia2:space_idx;
    mutable ia3:space_idx
}

type acell = {
    mutable a1:float;
    mutable a2:float;
    mutable a3:float;
}

type ('a,'b) t = {
    mutable c : 'a list;
    mutable acell : acell;
    mutable irp : irprimd;
    mutable elements : 'b list
}

let acell_of = function {c=_; irp=_; acell=a; elements=_} -> a
let irp_of = function {c=_; irp=a; acell=_;elements=_} -> a
let cells_of = function {c=cells; irp=_; acell=_;elements=_} -> cells
let elements_of = function {c=_; irp=_; acell=_;elements=e} -> e

let atom_symbols_in_pcell = function 
  {a0=a0;other=other;idx=idx}->
    let b = List.map (fun x -> x.name) other
    in a0.name::b

let get_symbols s =
  cells_of s
  |> List.map (fun x -> atom_symbols_in_pcell x )
  |> List.flatten

let create_default_crystal () =
  {
  c = [];
  acell={a1=1.0;a2=1.0;a3=1.0};
  irp={
  ia1={ix=1;iy=0;iz=0};
  ia2={ix=0;iy=1;iz=1};
  ia3={ix=0;iy=0;iz=0};
    };
    elements = [];
  }

let create_guard_atom name x y z = {name; x; y; z}

let create_other_atoms names positions guard_atom =
  let open List in
  map2 (fun name pos ->
      {name=name;
       dx = (nth pos 0) -. guard_atom.x;
       dy = (nth pos 1) -. guard_atom.y;
       dz = (nth pos 2) -. guard_atom.z;
      }
    )  names positions

let create_acell a1 a2 a3 = {a1;a2;a3}
let create_irp ia1 ia2 ia3 = {ia1;ia2;ia3}
let create_space_idx ix iy iz = {ix;iy;iz}
let create_pcell a0 other idx = {a0;other;idx}

let construct_crystal acell irp cells elements =
  { c=cells; acell=acell; irp=irp; elements=elements }


(** For direct print from crystal *)
let acell_print = function {a1=x;a2=y;a3=z} -> Printf.printf "%f %f %f\n" x y z

let irp_print = function
    {ia1=ia1;ia2=ia2;ia3=ia3}->
    List.iter (fun {ix=ix;iy=iy;iz=iz}
               -> Printf.printf "%d %d %d\n" ix iy iz) [ia1;ia2;ia3]


 (** For abinit printing *)
let guard_atom_print_abinit s oc =
  match s with {name=n; x=x; y=y; z=z} ->
    Printf.fprintf oc "%f %f %f\n" x y z

let atom_print_abinit atom guard_atom oc =
  match atom with 
    {name=n; dx=dx; dy=dy;dz=dz}->
    Printf.fprintf oc "%f %f %f\n"
                  (dx+.guard_atom.x) (dy+.guard_atom.y) (dz+.guard_atom.z)   

let pcell_print_abinit s oc = 
  match s with {a0=a0; other=other; idx=idx} ->
    guard_atom_print_abinit a0 oc;
      List.iter (fun x-> atom_print_abinit x a0 oc) other

let cells_print_abinit cells oc =
  List.iter (fun x -> pcell_print_abinit x oc) cells

let acell_print_abinit acell oc =
match acell with {a1=x;a2=y;a3=z} -> Printf.fprintf oc "%f %f %f\n" x y z

let irp_print_abinit irp oc = 
  match irp with {ia1=ia1;ia2=ia2;ia3=ia3}->
    List.iter (fun {ix=ix;iy=iy;iz=iz}
               -> Printf.fprintf oc "%d %d %d\n" ix iy iz) [ia1;ia2;ia3]


let rec random_move_atoms_in_pcell p range = 
  let a0 = p.a0 in
  let other_atoms = p.other in
  let nax = random_shift_guard_atom a0 range in
  let nother_atoms = List.map 
  (fun x -> random_shift_atom x range) other_atoms in
    { p with a0=nax; other=nother_atoms}
  and 
  random_shift_guard_atom a range =
      {name = a.name; 
       x = (r a.x range);
       y = (r a.y range);
       z = (r a.z range);
      }
  and 
  random_shift_atom a range =
      {name = a.name;
    dx = (r a.dx range); 
    dy = (r a.dy range);
    dz = (r a.dz range);
      }
  and r x range = x +. ( (Random.float 1.0) -. 0.5) *. range

