LIBS = str
PACKS = core yojson unix lacaml gsl
INCDIRS = +camlp4
export THREADS = y

RESULT = perocaml

#Order matters.
SOURCES = \
	gpr/utils.ml \
	gpr/block_diag.ml gpr/block_diag.mli \
	gpr/interfaces.ml \
  gpr/cov_const.ml gpr/cov_const.mli \
  \
	global_variables.ml \
	atomic_building_blocks.ml atomic_building_blocks.mli \
	alexer.mll aparser.mly \
	abinit.ml abinit.mli \
	crystal.ml \
	crystal_operations.ml \
	func_train_pairs.ml \
	main.ml

TRASH = *.cmi aparser.ml alexer.ml gpr/*.cmi gpr/*.cmo *.cmx gpr/*.cmx

OCAMLLEX =
OCAMLYACC = menhir
OCAMLMAKEFILE = OCamlMakefile
include $(OCAMLMAKEFILE)

loc:
	wc -l *.ml *.mli *.mly *.mll
