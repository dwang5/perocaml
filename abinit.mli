type abinit_related

val read_abinit_info : string -> abinit_related
val run_abinit : abinit_related -> int

val write_geometry :
  (Atomic_building_blocks.pcell, int * string * int * 'a)
  Atomic_building_blocks.t -> out_channel -> unit

val write_input :
  (Atomic_building_blocks.pcell, int * string * int * string)
  Atomic_building_blocks.t -> abinit_related -> unit

val write_files :
  (Atomic_building_blocks.pcell, int * string * int * string)
  Atomic_building_blocks.t -> abinit_related -> unit

val get_total_energy : abinit_related -> float
val parse : abinit_related -> unit
