# PeroCaML

Use ocaml to drive abinit calculation in order to generate training
datat sets for later use.

## Compile

Simply use 'make' on Linux/Unix platforms.

### Requirement

ocaml is necessary. Use 'opam' to install whatever missing packages
during compilation.


## Example

Please check the folder "example_bto" for more information.
