(*Target ABINIT 8.0.8*)

%{
open Printf
open Global_variables

%}

(*
%token  COLON
%token  SHARP
*)

%token  NEWLINE
%token <int>  INT
%token <float>  FLOAT
%token <string> STRING
%token  DT
%token  DTS
%token  E_CONV_BEGIN
%token  G_OUT_BEGIN
%token  END
%token  PRE_CMD
%token  EQPLUS
%token  EQPLUS2
%token  EQPLUS3
%token  SPACES

%start prog
%type <unit> prog

%%

prog:  { }
    | lines
      PRE_CMD dt_content 
      lines
      { }
  ;

dt_preface:
  | INT line
    xlines
    NEWLINE
    {print_string "|dt_preface|  "; print_int $1; print_string " "; 
    print_string $2; print_string "\n"; print_string $3; print_endline}
  | INT line 
    xlines
    NEWLINE
    NEWLINE
    DT
    dt_preface {print_string "|dt_preface| "; print_int $1; print_string " "; 
    print_string $2; print_string "\n"; print_string $3; print_endline}
  ;

xlines: { "" }
  | line { $1 }
  | line xlines { $1^"\n"^$2  }
  ;

dt_content:
  | DT INT EQPLUS2 NEWLINE dt_content_lines dt_end { "" }
  | DT INT EQPLUS2 NEWLINE (*This needs further check*)
    dt_content_lines
    NEWLINE 
    PRE_CMD dt_content dt_end { "" }
  ;

(*lines inside a dataset*)
dt_content_lines:
  | line { $1 }
  | NEWLINE { "" }
  | line lines { $1^$2 }
  | NEWLINE lines { $2 }
  ;

dt_end:
  | PRE_CMD END DTS EQPLUS3 NEWLINE { "" }
  ;

lines: { "" }
  | line { $1 }
  | line lines { $1^$2 }
  ;

line:      { "" }
  | NEWLINE { "" }
  | SPACES NEWLINE { "" }
  | STRING NEWLINE { "" }
  | STRING line { "" }
  | INT NEWLINE { "" }
  | INT line { "" }
  | FLOAT NEWLINE { etot_in_hartree:=$1; sting_of_float $1 }
  | FLOAT line { etot_in_hartree:=$1; string_of_float $1 }
  ;


%%
