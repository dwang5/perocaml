type abinit_related = {
  mutable mpi_command : string;
  mutable abinit_command : string;
  mutable number_processes : int;
  mutable files: string;
  mutable input : string;
  mutable output : string;
  mutable log : string;
  mutable info_set : bool;
  mutable abinit_boiler_plate : string list;
}


let read_abinit_info abinit_config_file =
  (* Read the JSON file *)
  let json = Yojson.Basic.from_file abinit_config_file in
  (* Locally open the JSON manipulation functions *)
  let open Yojson.Basic.Util in
  let mc = json |> member "mpi_command" |> to_string in
  let ac = json |> member "abinit_command" |> to_string in
  let np = json |> member "number_processes" |> to_int in
  let files = json |> member "files" |> to_string in
  let inp = json |> member "input" |> to_string in
  let out = json |> member "output" |> to_string in
  let log = json |> member "log" |> to_string in
  let boiler_plate = json |> member "abinit_boiler_plate" 
  |> to_list |> filter_string in
  let x = { mpi_command = mc; abinit_command = ac; 
  number_processes = np; 
  files = files;
  input = inp; output = out; 
  log = log; info_set = true; 
  abinit_boiler_plate = boiler_plate} in
  x

let rec run_abinit abinit_state =
  let open Unix in
  let state = run_abinit1 abinit_state in
  match state with
    | WEXITED 0 -> 1
    | _ -> 0
and run_abinit1 (arg:abinit_related) =
  (** Printf.printf "%s %s %s\n"
     arg.mpi_command
     arg.abinit_command
     arg.files;
     *)
  let state =  
    Core.String.concat ~sep:" "  
    [arg.mpi_command; 
    "-np"; 
    (string_of_int arg.number_processes);
    arg.abinit_command; "<"; arg.files; ">"; arg.log]
    |> Unix.system
  in state

let write_boiler_plate abinit_state oc = 
  List.iter (fun x-> Printf.fprintf oc "%s \n" x ) 
  abinit_state.abinit_boiler_plate

let write_geometry s oc =
  let open Atomic_building_blocks in
  let open Printf in
  let atom_names = get_symbols s in
  let acell = acell_of s  in
  let irp = irp_of s  in
  let natom = List.length atom_names in
  let elements = elements_of s in
  let cells = cells_of s in
  let znucl = List.map (fun (x,y,z,u) -> z) elements in
  let ename = List.map (fun (x,y,z,u) -> (y,x)) elements in
  let typat = List.map (fun x -> List.assoc x ename) atom_names in
  fprintf oc "natom %d \n" natom;
  fprintf oc "znucl "; List.iter (fprintf oc "%d ") znucl; fprintf oc "\n";
  fprintf oc "ntypat %d\n" (List.length znucl);
  fprintf oc "typat "; List.iter (fprintf oc "%d ") typat; fprintf oc "\n";
  fprintf oc "acell "; acell_print_abinit acell oc;
  fprintf oc "rprim\n"; irp_print_abinit irp oc;
  fprintf oc "xred\n"; cells_print_abinit cells oc

let write_input s abinit_state =
  let oc = open_out abinit_state.input in
  let () = write_geometry s oc in
  let () = write_boiler_plate abinit_state oc
  in close_out oc

let write_files s abinit_state =
  let oc = open_out abinit_state.files in
  let open Printf in
  let open Atomic_building_blocks in
  let psp = elements_of s |> List.map (fun (i,n,z,p) -> p ) in
  let () = begin
  fprintf oc "%s \n" abinit_state.input;
  fprintf oc "%s \n" abinit_state.output;
  fprintf oc "%s \n" "soft_i";
  fprintf oc "%s \n" "soft_o";
  fprintf oc "%s \n" "soft_x";
  List.iter (fun x -> fprintf oc "%s\n" x) psp
  end
  in close_out oc

let get_total_energy abinit_state =
  0.0

let parse abinit_state =
  let oc = open_in abinit_state.output in
  let () = begin
  try 
    let lexbuf = Lexing.from_channel oc in
    while true do
      try
        Aparser.prog Alexer.read lexbuf
      with 
      | Alexer.Error msg -> Printf.eprintf "%s%!" msg
      | Aparser.Error -> Printf.eprintf "At offset %d: syntax error. \n%!" (Lexing.lexeme_start lexbuf)
    done
  with End_of_file -> ()
  end
  in close_in oc

