(** Contians all the functionalities *)
open Global_variables

let rec generate_pairs n s0 abinit_state range =
  let rec generation_helper i s =
    if i>=n  then ()
    else
      begin
        let fn = "pair_" ^ (string_of_int i) in
        if Sys.file_exists fn then Unix.unlink fn;
        let oc = open_out fn in
        let () = Abinit.write_geometry s oc in
        let () = Abinit.write_input s abinit_state in
        let _  = Abinit.run_abinit abinit_state in
        let () = Abinit.parse abinit_state in
        let () = Printf.fprintf oc "---\n %s %f \n" "Etotal (Hartree): " !etot_in_hartree in
        let _ =  close_out oc in
        let open Crystal_operations in
        let s1 = random_move_all_atoms s0 range in
        generation_helper (i+1) s1
      end
  in
  generation_helper 0 s0
