(* This file will contain operations on crystals that will be used by GP and other algorithms
 * The intention here is to write some high level routines.
 * *)

type dir = X | Y | Z

type strain = {
  eta1: float;
  eta2: float;
  eta3: float;
  eta4: float;
  eta5: float;
  eta6: float;
}

type operation = 
  | Guard_atom of int * float * float * float (* This will also determine local strain *)
  | Other_atom of int * float * float * float
  | Strain of strain

  (* operate operation o on the abinit state s *)
  (* s is the type Crystal.read *)
let operate o s = 
  let open Atomic_building_blocks in
  let open Printf in
  let cells = cells_of s in
  match o with
  | Guard_atom (i,dx,dy,dz) ->
      let c = List.nth cells i in
      pcell_print_abinit c stdout
  | Other_atom (i,dx,dy,dz) -> ()
  | Strain eta -> ()

  (** This operation randomly shfit all atoms within a specified range. 
   * The function will be used to generate training data set (x, d) for
   * later neural network *)
let random_move_all_atoms s range = 
  let open Atomic_building_blocks in
  let open Printf in
  let cells = cells_of s in
  let acell = acell_of s  in
  let irp = irp_of s  in
  let elements = elements_of s in
  let ncells = List.map (fun pcell -> random_move_atoms_in_pcell pcell range) cells in
  construct_crystal acell irp ncells elements
