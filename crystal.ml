open Atomic_building_blocks


(* Parse the cells part from json file*)
let parse_pcell json =
  let open Yojson.Basic.Util in
  let open List in
  let idx =
    json |> member "idx"
    |> to_list
    |> filter_int in
  let names =
    json |> member "name"
    |> to_list
    |> filter_string in
  let positions =
    json |> member "positions"
    |> to_list
    |> List.map (fun x -> x |> to_list |> filter_float ) in
  let guard_atom = create_guard_atom
                     (nth names 0)
                     (nth (nth positions 0) 0)
                     (nth (nth positions 0) 1)
                     (nth (nth positions 0) 2) in
  let other =  create_other_atoms (tl names) (tl positions) guard_atom in                
  create_pcell guard_atom other (create_space_idx (nth idx 0) (nth idx 1) (nth idx 2))

let parse_cells json =
  let open Yojson.Basic.Util in
  let cells = json |> member "cells"
              |> to_list
              |> List.map (fun x -> parse_pcell x)
  in cells

let parse_acell json =
  let open List in
  let open Yojson.Basic.Util in
  let a = json|> member "acell"|> to_list|> filter_float in
  create_acell (nth a 0) (nth a 1)  (nth a 2)

let parse_irp json =
  let open List in
  let open Yojson.Basic.Util in
  let ia =
    List.map
      (fun x -> (member x json)
                |> to_list
                |> filter_int)
      ["ia1"; "ia2"; "ia3"]
    |> List.map (fun x -> create_space_idx (nth x 0) (nth x 1) (nth x 2) ) in
  create_irp (nth ia 0) (nth ia 1) (nth ia 2)

let process_element i json =
  let open Yojson.Basic.Util in
  let open List in
  let name = json |> member "name" |> to_string in
  let nZ = json |> member "Z" |> to_int in
  let psp = json |> member "psp" |> to_string in
  (i, name, nZ, psp)

let print_element_info =function
  (i, name, nZ, psp) -> Printf.printf "idx: %d; name: %s; nZ: %d; psp: %s\n" i name nZ psp

let read_elements_info json = 
  let open Yojson.Basic.Util in
  let elements = json 
            |> member "elements"
            |> to_list
            |> List.mapi (fun i x-> process_element (i+1) x)
  in elements

(** Read the crystal from json file*) 
let read cfg =
  (* Read the JSON file *)
  let json = Yojson.Basic.from_file cfg in
  (* Locally open the JSON manipulation functions *)
  let open Yojson.Basic.Util in
  let a = parse_acell json in
  let irp = parse_irp json in
  let cells = parse_cells json in
  let elements = read_elements_info json in
  construct_crystal a irp cells elements

let print s =
  let a = acell_of s in
  let irp = irp_of s in
  let () =
    begin
      print_endline "--acell--";
      acell_print a;
      print_endline "--irp--";
      irp_print irp;
    end
  in ()

