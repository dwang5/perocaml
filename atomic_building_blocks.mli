type guard_atom 

type atom 
type space_idx 
type pcell
type irprimd 
type acell

type ('a,'b) t

val acell_of : ('a, 'b) t -> acell
val irp_of : ('a, 'b) t -> irprimd
val cells_of : ('a, 'b) t -> 'a list
val elements_of : ('a, 'b) t -> 'b list
val acell_print : acell -> unit
val irp_print : irprimd -> unit
val atom_symbols_in_pcell : pcell -> string list
val get_symbols : (pcell, 'a) t -> string list
val create_default_crystal : unit -> ('a, 'b) t
val create_guard_atom : string -> float -> float -> float -> guard_atom
val create_other_atoms :
  string list -> float list list -> guard_atom -> atom list
val create_acell : float -> float -> float -> acell
val create_irp : space_idx -> space_idx -> space_idx -> irprimd
val create_space_idx : int -> int -> int -> space_idx
val create_pcell : guard_atom -> atom list -> space_idx -> pcell
val construct_crystal : acell -> irprimd -> 'a list -> 'b list -> ('a, 'b) t
val cells_print_abinit : pcell list -> out_channel -> unit
val acell_print_abinit : acell -> out_channel -> unit
val irp_print_abinit : irprimd -> out_channel -> unit

val pcell_print_abinit : pcell -> out_channel -> unit
val random_move_atoms_in_pcell : pcell -> float -> pcell
