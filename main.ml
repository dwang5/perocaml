open Global_variables

(** Crystal information *)
let s = Crystal.read "crystal.json"
(** Abinit information, e.g., the abinit program to run ... *)
let abinit_state = Abinit.read_abinit_info "judge_abinit.json"
(** abinit input .files file
 * bto.files are always the same*)
let _ = Abinit.write_files s abinit_state

let () = 
  let open Func_train_pairs in
  generate_pairs 2000 s abinit_state 0.3





  (* let () =  *)
    (* let total_energy = Abinit.get_total_energy abinit_state in *)
    (* print_float total_energy; print_endline "" *)
