(*Target ABINIT 8.0.8*)

{
  open Aparser 
  open Printf
  exception Error of string

let create_hashtable size init =
  let tbl = Hashtbl.create size in
  List.iter (fun (key, data) -> Hashtbl.add tbl key data) init; tbl

let keyword_table = 
  create_hashtable 30 [
    ("DATASET", DT);
    ("DATASET(S)", DTS);
    ("---SELF-CONSISTENT-FIELD CONVERGENCE--------------------------------------------", E_CONV_BEGIN);
    ("---OUTPUT-----------------------------------------------------------------------", G_OUT_BEGIN);
    ("END", END);
    ("==", PRE_CMD);
    ("  ", SPACES);
    ("================================================================================",EQPLUS);
    ("==================================================================",EQPLUS2);
    ("==============================================================",EQPLUS3) (*After 'END DATASETS'*)
 ]
}

let digit = ['0'-'9']
let frac = '.' digit*
let numexp = ['e' 'E'] ['-' '+']? digit+*
let float = ['-' '+']? digit * frac? numexp?
let int = digit+ (* This def is wrong *)

let white = [' ' '\t']+
let space = "  "
let newline = '\r' | '\n' | "\r\n"
let astring = ['a'-'z' 'A'-'Z' '0'-'9' '_'  '.' '/' ]+
let precmd = "== "
let eqs = "================================================================================"
let rhs = ">>>>>>>>>"
let etotal = "Etotal="

(*all possible symbols used in abinit output*)
let abinit_string = ['a'-'z' 'A'-'Z' '0'-'9' '_'  '.' '/' '@' '!' '=' '#' '&' '+' '-'
  '{' '}' '[' ']' '(' ')' '<' '>' '^' ':' ';' '?' ',' '%' '~' '*' '$' '`' '\'' '\"']+

rule read = parse
  | white	{ read lexbuf }
  | space newline { NEWLINE }
  | newline	{ NEWLINE }
  | eqs newline { NEWLINE }
  | rhs white etotal white (float as num) { FLOAT (float_of_string num) }
  | int as num { INT  (int_of_string num) }
  | abinit_string as str {
    try
      let tok = Hashtbl.find keyword_table str in tok
    with Not_found -> STRING str
  }
  | eof		{ raise End_of_file }
